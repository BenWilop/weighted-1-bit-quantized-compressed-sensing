import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from scipy.spatial import Voronoi, voronoi_plot_2d

def plot_voronio_regions(Q):
    """
    plots voronio regions for quantizerset Q
    :param Q: image of quantizer
    :return:
    """
    vor = Voronoi(Q)
    voronoi_plot_2d(vor)
    plt.xlim([-2.1, 2.1]), plt.ylim([-2.1, 2.1])
    plt.title("Voronoi regions of grid")
    plt.show()

def plot_voronio_regions_zoomed_out(Q):
    """
    plots voronio regions for quantizerset Q with empty space around
    :param Q: image of quantizer
    :return:
    """
    vor = Voronoi(Q)
    voronoi_plot_2d(vor)
    plt.xlim([-5, 5]), plt.ylim([-5, 5])
    plt.title("Voronoi regions of grid zoomed out")
    plt.show()

def plot_unit_ball_weighted_p_norm(p, w):
    # Code is based on the explanation and examples from
    # https://mimmackk.github.io/unitball/
    """
    plots unit-ball in p-norm, i.e. all 1-sparse points
    :param w: weights
    :return:
    """
    figure, axes = plt.subplots(1)

    # p_norm
    angle = np.linspace(0, 2 * np.pi, 2 ** 20 + 1)
    radius = (np.abs(np.sin(angle)) ** p + np.abs(np.cos(angle)) ** p) ** (-1 / p)
    x = radius * np.cos(angle)
    y = radius * np.sin(angle)
    axes.plot(x, y)

    # w-p-norm
    radiusW = (w[0] ** (2 - p) * np.abs(np.cos(angle)) ** p + w[1] ** (2 - p) * np.abs(np.sin(angle)) ** p) ** (
                -1 / p)
    xW = radiusW * np.cos(angle)
    yW = radiusW * np.sin(angle)
    axes.plot(xW, yW)

    axes.set_aspect(1)
    plt.title("p={}, w={}".format(p,w))
    plt.show()

def plot_unit_ball_weighted_0_norm(w):
    # Code is based on the explanation and examples from
    # https://mimmackk.github.io/unitball/
    """
    plots unit-ball in 0-norm, i.e. all 1-sparse points
    :param w: weights
    :return:
    """
    figure, axes = plt.subplots(1)

    # x-axis
    x1 = np.linspace(-1, 1, 2**20+1)
    y1 = np.linspace(0, 0, 2**20+1)
    if w[0] == 1: # w-0_norm
        axes.plot(x1, y1, "C1")
    else: # 0_norm
        axes.plot(x1, y1, "C0")

    x2 = np.linspace(0, 0, 2 ** 20 + 1)
    y2 = np.linspace(-1, 1, 2 ** 20 + 1)
    axes.plot(x2, y2, "blue")
    if w[1] == 1: # w-0_norm
        axes.plot(x2, y2, "C1")
    else: # 0_norm
        axes.plot(x2, y2, "C0")

    axes.set_aspect(1)
    plt.title("p=0, w={}".format(w))
    plt.show()

def norm_w_one(x, w):
    """
    calculated weighted one-norm of x
    :param x: vector
    :param w: weights
    :return: weighted one-norm of x ||x||_w,1
    """
    return w[0]*np.abs(x[0])+w[1]*np.abs(x[1])

def plot_effective_sparse(s,w):
    """
    plots the set of effectively sparse points
    blue for unweighted, orange for weights
    :param s: sparsity level
    :param w: weights
    :return:
    """
    figure, axes = plt.subplots(1)
    for i in range(2**17):
        x = np.array([np.random.rand() * 2 - 1, np.random.rand() * 2 - 1])
        if norm_w_one(x, w) <= np.sqrt(s)*np.linalg.norm(x,2): # w-s-effective sparse
            axes.plot(x[0], x[1], marker='.', color="C1",  markersize = "1")
        elif np.linalg.norm(x,1) <= np.sqrt(s)*np.linalg.norm(x,2): # s-effective sparse
            axes.plot(x[0], x[1], marker='.', color="C0", markersize = "1")

    axes.set_aspect(1)
    plt.title("s={}, w={}".format(s,w))
    plt.show()

def unitball3d(p, w, smoothness = 6):
    """
    plots the 3d-unitballs
    :param p: norm
    :param w: weights
    :param smoothness: determines the number of points to use for the meshgrid
    :return:
    """
    num_steps = 2**smoothness + 1
    theta = np.linspace(0, np.pi * 2, num_steps)
    phi   = np.linspace(0, np.pi,     num_steps)
    theta, phi = np.meshgrid(theta, phi)

    r = (w[2]**(2-p)*np.abs(np.cos(phi))**p + np.sin(phi)**p * (w[0]**(2-p)*np.abs(np.cos(theta))**p
    + w[1]**(2-p)*np.abs(np.sin(theta))**p))**(-1/p)
    x = r * np.sin(phi) * np.cos(theta)
    y = r * np.sin(phi) * np.sin(theta)
    z = r * np.cos(phi)

    fig = go.Figure(go.Surface(x=x, y=y, z=z, colorscale='Viridis')) #showscale=False,
    fig.update_layout(height=600, scene_aspectmode='data', title = go.layout.Title(
        text="p={}, w={}".format(p,w)))
    fig.show()

def plot_diff_weighted_1_and_2_norm(w):
    figure, axes = plt.subplots(1)
    #axes.grid(which='both', color='grey', linewidth=1, linestyle='-', alpha=0.2)
    plt.axis([-1, 1, -1, 1])
    plt.grid()
    plt.gca().set_aspect("equal")

    # 2_norm
    angle = np.linspace(0, 2 * np.pi, 2 ** 20 + 1)
    radius = (np.abs(np.sin(angle)) ** 2 + np.abs(np.cos(angle)) ** 2) ** (-1 / 2)
    x = radius * np.cos(angle)
    y = radius * np.sin(angle)
    axes.plot(x, y)

    # w-1-norm
    radiusW = (w[0] * np.abs(np.cos(angle)) + w[1] * np.abs(np.sin(angle)))**(-1)
    xW = radiusW * np.cos(angle)
    yW = radiusW * np.sin(angle)
    axes.plot(xW, yW)
    # ray through w
    t = np.linspace(-1/np.max(w), 1/np.max(w), 2 ** 20 + 1)
    xR = w[0]*t
    yR = w[1]*t
    axes.plot(xR, yR)

    axes.set_aspect(1)
    plt.title("w={}".format(w))
    plt.show()

def plot_weighted_recovery():
    """
    plots example
    :return:
    """
    w = [1,1]
    figure, axes = plt.subplots(1)
    # axes.grid(which='both', color='grey', linewidth=1, linestyle='-', alpha=0.2)
    plt.axis([-1.2, 1.2, -1.2, 1.2])
    plt.grid()
    plt.gca().set_aspect("equal")

    # w-1-norm
    angle = np.linspace(0, 2 * np.pi, 2 ** 20 + 1)
    radiusW = 0.5*(w[0] * np.abs(np.cos(angle)) + w[1] * np.abs(np.sin(angle))) ** (-1)
    xW = radiusW * np.cos(angle)
    yW = radiusW * np.sin(angle)
    axes.plot(xW, yW)

    # Ker(A) defined by k
    k = [1,-1.5]
    t = np.linspace(-1 / np.max(w), 1 / np.max(w), 2 ** 20 + 1)
    xR = k[0] * t + 0.5
    yR = k[1] * t
    axes.plot(xR, yR)

    axes.set_aspect(1)
    plt.title("l_1 minimization with regular unit ball")
    plt.show()

def plot_weighted_recovery_distorted():
    """
    plots exemplary recovery with a distorted and regular unitball,
    i.e. the minimally blown up unit-ball that touches the kernel
    :return:
    """
    w = [2,1]
    figure, axes = plt.subplots(1)
    # axes.grid(which='both', color='grey', linewidth=1, linestyle='-', alpha=0.2)
    plt.axis([-1.2, 1.2, -1.2, 1.2])
    plt.grid()
    plt.gca().set_aspect("equal")

    # w-1-norm
    angle = np.linspace(0, 2 * np.pi, 2 ** 20 + 1)
    radiusW = 0.75*(w[0] * np.abs(np.cos(angle)) + w[1] * np.abs(np.sin(angle))) ** (-1)
    xW = radiusW * np.cos(angle)
    yW = radiusW * np.sin(angle)
    axes.plot(xW, yW)

    # Ker(A) defined by k
    k = [1,-1.5]
    t = np.linspace(-10, 10, 2 ** 20 + 1)
    xR = k[0] * t + 0.5
    yR = k[1] * t
    axes.plot(xR, yR)

    axes.set_aspect(1)
    plt.title("l_1 minimization with distorted unit ball")
    plt.show()

def plot_hard_threshold(x):
    """
    plots an example of the hard thresholding operator with lines for the projection and coordinates
    :param x: vector
    :return:
    """
    figure, axes = plt.subplots(1)
    plt.axis([-1, 1, -1, 1])
    plt.grid()
    plt.gca().set_aspect("equal")

    # x-axis
    x1 = np.linspace(-1, 1, 2**20+1)
    y1 = np.linspace(0, 0, 2**20+1)
    axes.plot(x1, y1, "C0")

    # y-axis
    x2 = np.linspace(0, 0, 2 ** 20 + 1)
    y2 = np.linspace(-1, 1, 2 ** 20 + 1)
    axes.plot(x2, y2, "C0")

    # lines
    plt.plot([x[0], 0], [x[1],x[1]], 'bo', linestyle="--")
    plt.plot([x[0], x[0]], [0, x[1]], 'bo', linestyle="--")

    # H_s(x)
    if abs(x[0]) >= abs(x[1]):
        plt.text(x[0] - 0.3, -0.1, "H_s({})".format(x))
    else:
        plt.text(0.015, x[1] + 0.25, "H_s({})".format(x))

    axes.set_aspect(1)
    plt.title("x=[1, 2]")
    plt.show()

def plot_f():
    """
    plots the function f on the interval [0,10] in the proof of CP
    :return:
    """
    figure, axes = plt.subplots(1)
    x = np.linspace(0, 10, 2**20+1)
    y = 4*x+4-8*np.sqrt(x)
    axes.plot(x, y)
    axes.set_aspect(1)
    plt.title('f')
    plt.show()

def plot_g():
    """
    plots the function g on the interval [-0.1,0.1] in the proof of CP
    :return:
    """
    figure, axes = plt.subplots(1)
    x = np.linspace(-0.1, 0.1, 2**20+1)
    y = (0.25-1.75*x)/(1+x)
    axes.plot(x, y)
    axes.set_aspect(1)
    plt.title('g')
    plt.show()

Q = np.array([[0, 0], [1, 0], [-1, 0], [0, 1], [0, -1], [1, 1], [1, -1], [-1, 1], [-1, -1], [0,2], [2,0], [0,-2], [-2,0]])
#plot_voronio_regions(Q)
#plot_voronio_regions_zoomed_out(Q)

plot_weighted_recovery()
plot_weighted_recovery_distorted()

p = 1
w2 = [2, 1]
# plot_unit_ball_weighted_0_norm(w2)
#plot_unit_ball_weighted_p_norm(p,w2)
# plot_effective_sparse(p,w2)
#plot_diff_weighted_1_and_2_norm(w2)

w3 = [2, 1.5, 1]
#unitball3d(p, w3)

x= [0.7, 0.5]
#plot_hard_threshold(x)

#plot_f()
#plot_g()