import numpy as np

def signal_first_k(n, k):
    """
    constructs random signal that has value 1 for the first k and value 0 for all other components.
    After that gaussian noise is added in order to construct an only effectively sparse vector.
    :param n: dimension of x R^n
    :param k: number of main components
    :param noise_level: standard derivation of gaussian noise
    :return: simulated signal x
    """
    signal = np.zeros(n)
    supp_prob = [1/(2*k) for i in range(k)] + [1/(2*(n-k)) for i in range(n-k)]
    supp = np.random.choice(range(n), k, p=supp_prob)
    signal[supp] = np.ones(k) + np.random.uniform(0,1, size = k)
    signal[0:k] = 1
    signal /= np.linalg.norm(signal)
    return signal