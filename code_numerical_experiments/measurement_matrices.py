import numpy as np
import scipy as sp


def gaussian_measurement_matrix(m,n):
    """
    calculates a gaussian measurement matrix
    :param m: number of rows
    :param n: number of columns
    :return: matrix A
    """
    return np.random.normal(0, 1, size=(m, n)) * (np.sqrt(np.pi / 2) / m) #1/sqrt{m} worked better for Cone-Program


def gaussian_circulant_measurement_matrix(m,n):
    """
    calculates a gaussian measurement matrix
    :param m: number of rows
    :param n: number of columns
    :return: matrix A
    """
    g = np.random.normal(0, 1, size=n)
    G = sp.linalg.circulant(g)
    #indices = (np.random.uniform(0,1,n) <= m/n)
    indices = np.random.choice(range(n), m, replace=False)
    A = G[indices,:] * (np.sqrt(np.pi / 2) / m) #1/m worked better for Cone-Program
    return A


def real_trigonometric_polynomials_measurement_matrix(m,n, sample_times):
    """
    calculates a real trigonometric polynomial measurement matrix
    frequencies: {1, sin1, cos1, sin2, cos2, ..., sinq, cosq}
    :param m: number of rows
    :param n: number of columns
    :return: matrix A
    """
    A = np.zeros((m,n))
    q = int((n-1)/2)
    for (i,t) in enumerate(sample_times):
        A[i,0] = 1 #np.sqrt(2)
        for j in range(1,q+1):
            A[i, 2 * j] = np.sqrt(2) * np.cos(2 * np.pi * j * t)
            A[i, 2 * j - 1] = np.sqrt(2) * np.sin(2 * np.pi * j * t)
    A *= (np.sqrt(np.pi / 2) / m)
    print("A: ", A)
    return A


def bernoulli_measurement_matrix(m,n):
    """
    calculates a bernoulli measurement matrix
    :param m: number of rows
    :param n: number of columns
    :return: matrix A
    """
    A = 2*np.random.binomial(1,0.5,size=(m,n)) - np.ones((m,n))
    A *= (np.sqrt(np.pi / 2) / m)
    return A