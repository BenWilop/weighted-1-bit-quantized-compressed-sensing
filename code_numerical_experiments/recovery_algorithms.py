import time
import numpy as np
import cvxpy as cvx

def solve(vx, objective, constraints):
    """
    solves optimization problem
    :param vx: variable to optimize
    :param objective: cost function
    :param constraints: constraints that define feasible set
    :return: solution
    """
    prob = cvx.Problem(objective, constraints)
    result = prob.solve(verbose=True) # other solvers: solver=cvx.CVXOPT, solver=cvx.SCIPY)
    end = time.time()
    x = np.array(vx.value)
    x = np.squeeze(x)
    return x

def linear_program(A, y, w):
    """
    solves the linear program
    :param A: measurement matrix R^mxN
    :param y: (quantized) measurement \sng(y=Ax) with ||x||_2=1 R^m
    :param w: weights 1 <= w_j R^N
    :return: solution of LP R^N
    """
    # rewrite the problem in the form x = x^- + x^+
    n = A.shape[1]
    vx = cvx.Variable(2*n)
    w = np.concatenate((w, w), axis=0)
    objective = cvx.Minimize(cvx.pnorm(cvx.multiply(w, vx), 1)) # min ||x||_w,1
    constraints = [vx >= 0, cvx.multiply(A @ vx[:n] - A @ vx[n:], y) >= 0,
                   cvx.sum(cvx.multiply(A @ vx[:n] - A @ vx[n:], y)) == 1] # sgn(Ax) = y, ||Ax||_1 = 1
    x = solve(vx, objective, constraints)
    solution = x[:n] - x[n:]
    return solution

def cone_program(A, y, w, R):
    """
    solves the cone program
    :param A: measurement matrix R^mx(N+1)
    :param y: (quantized) measurement y=sgn(A[x,R]) with ||x||_2<=R R^m
    :param w: weights 1 <= w_j RN
    :param R: bound on ||x||_2<=R
    :return: solution of CP R^N
    """
    # rewrite the problem in the form x = x^- + x^+
    n = A.shape[1]-1
    vx = cvx.Variable(2*n)
    w = np.concatenate((w, w), axis=0)
    objective = cvx.Minimize(cvx.pnorm(cvx.multiply(w, vx), 1)) # min ||x||_w,1
    constraints = [vx >= 0, cvx.multiply(A[:,:n] @ vx[:n] - A[:,:n] @ vx[n:] + A[:,n]*R, y) >= 0,
                   cvx.norm(vx, 2) <= R*R] # sgn(A[x,1]) = y, ||x||_2 <= R
    x = solve(vx, objective, constraints)
    solution = x[:n] - x[n:]
    return solution

def hard_thresholding_operator(A, y, w, s):
    """
    Calculates the hard thresholding operator H_s,w(x)
    :param A: measurement matrix R^mxN
    :param y:  (quantized) measurement sgn(y=Ax) with ||x||_2=1 R^m
    :param w: weights 1 <= w_j R^N
    :return: solution of HS R^N
    """
    # rewrite the problem in the form x = x^- + x^+
    n = A.shape[1]
    x = np.matmul(np.transpose(A), y)

    # initialization of m
    m = np.zeros((n+1,s+1))
    # filling m
    for i in range(1,n+1):
        for j in range(s+1):
            if w[i-1] > j:
                m[i,j] = m[i-1,j]
            else:
                m[i,j] = max(m[i-1,j], m[i-1,j-int(w[i-1])]+x[i-1]**2)
    # Calculation of result
    z = np.zeros(n)
    i = n
    j = s
    while i != 0:
        if m[i,j] > m[i-1,j]:
            z[i-1] += x[i-1]
            j -= int(w[i-1])
        i -= 1

    # return result
    solution = z
    end = time.time()
    return solution