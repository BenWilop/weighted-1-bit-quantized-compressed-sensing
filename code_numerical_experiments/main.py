from measurement_matrices import *
from recovery_algorithms import *
from plot_recovery_results import *
from create_signal import *
from scipy import signal

import numpy as np
import scipy.optimize as spopt
import scipy.fftpack as spfft
import scipy.ndimage as spimg
import cvxpy as cvx
from PIL import Image


def reconstruct_weights_number_measurements(measurement_matrix, recovery_algorithm):
    """
    Comparison of different algorithm x weight x matrix combinations.
    Creates a random signal and recovers it with multiple matrices
    :param measurement_matrix:
    :param recovery_algorithm:
    :return:
    """
    # parameters
    number_of_experiments = 5
    n = 501
    number_of_measurements = np.linspace(50,1000,20).round().astype(int)
    #number_of_measurements = np.linspace(50, 500, 10).round().astype(int)
    k = 10
    w = np.ones(n)*10
    w[0:k] = 1

    # perform experiments
    x_error_vector = [[] for (m_i, m) in enumerate(number_of_measurements)]
    x_w_error_vector = [[] for (m_i, m) in enumerate(number_of_measurements)]
    for (m_i, m) in enumerate(number_of_measurements):
        for i in range(number_of_experiments):
            signal = signal_first_k(n, k)

            # measurement matrix
            if measurement_matrix == "gaussian":
                A = gaussian_measurement_matrix(m, n)
            elif measurement_matrix == "gaussian_circulant":
                A = gaussian_circulant_measurement_matrix(m, n)
            elif measurement_matrix == "real_trigonometric_polynomials":
                sample_times = np.random.uniform(0, 1, size=m)
                A = real_trigonometric_polynomials_measurement_matrix(m, n, sample_times)
            elif measurement_matrix == "bernoulli":
                A = bernoulli_measurement_matrix(m, n)

            # measurement
            if recovery_algorithm in  ["HS", "LP"]:
                y = np.sign(np.matmul(A, signal))  # y=sgn(Ax)
            elif recovery_algorithm == "CP":
                R = np.linalg.norm(signal)
                h = np.random.normal(0, 1, size=m) * (np.sqrt(np.pi / 2) / m)
                A = np.c_[A, h]
                y = np.sign(np.matmul(A, np.append(signal, R)))  # y=sgn(Ax)

            # solve
            x = 0
            x_w = 0
            if recovery_algorithm == "HS":
                s = 10
                x = hard_thresholding_operator(A, y, np.ones(n), s)
                s = 65
                x_w = hard_thresholding_operator(A, y, w, s)
            elif recovery_algorithm == "LP":
                x = linear_program(A, y, np.ones(n))
                x_w = linear_program(A, y, w)
            elif recovery_algorithm == "CP":
                x = cone_program(A, y, np.ones(n), R)
                x_w = cone_program(A, y, w, R)

            # calculate and append values
            x_error = np.linalg.norm(x-signal) / np.linalg.norm(signal)
            x_error_vector[m_i].append(x_error)
            x_w_error = np.linalg.norm(x_w-signal) / np.linalg.norm(signal)
            x_w_error_vector[m_i].append(x_w_error)

    # plot results
    plot_weights_number_measurements(n, number_of_measurements, measurement_matrix, recovery_algorithm,
                                     x_error_vector, x_w_error_vector)


def reconstruct_function(recovery_algorithm):
    """
    interpolates function, measures only if value is above / below threshold

    :param recovery_algorithm:
    :return:
    """
    # parameters
    n = 501
    m = 250
    k = 10
    s = 16
    number_of_experiments = 1
    R = 1
    w = np.ones(n)
    q = int((n - 1) / 2)
    for j in range(1,q+1):
        w[2 * j] = j#np.log(j+1)
        w[2 * j - 1] = j#j**2#np.log(j+1)

    # function
    def f(x):
        #return np.sin(2 * np.pi * x) + np.sin(4 * np.pi * x) + np.sin(6 * np.pi * x) + np.sin(8 * np.pi * x)
        #return 2 / (1+25*x**2) - 1
        #return 4*np.abs(x)-1
        #return (np.abs(x) > 0.2) - 0.5
        #return signal.sawtooth(2 * np.pi * 5 * x, 0.5)
        return signal.gausspulse(2 * np.pi * 5 * x, 0.1)

    x_w_vector = []
    for i in range(number_of_experiments):
        # create measurement matrix
        sample_times = np.random.uniform(-0.5, 0.5, size=m)
        sample_times.sort()

        A = real_trigonometric_polynomials_measurement_matrix(m, n, sample_times)

        # take measurements
        if recovery_algorithm in ["HS", "LP"]:
            h = np.zeros(m)
            y = np.sign(f(sample_times))  # y=sgn(Ax)
        elif recovery_algorithm == "CP":
            h = np.random.normal(0, 1, size=m)
            A = np.c_[A, h * (np.sqrt(np.pi / 2) / m)]
            y = np.sign(f(sample_times) + R * h )

        # reconstruct function
        if recovery_algorithm == "HS":
            x_w = hard_thresholding_operator(A, y, w, s)
        elif recovery_algorithm == "LP":
            x_w = linear_program(A, y, w)
        elif recovery_algorithm == "CP":
            x_w = cone_program(A, y, w, R)

        # save data
        x_w_vector.append(x_w)

    # plot results
    plot_recovered_function(f, m, recovery_algorithm, "\n gaussian puls",
                            x_w_vector, sample_times, h, R) # w_1=1, w_2j-1=j, w_2j=j,


def dct2(x): # column wise
    return spfft.dct(spfft.dct(x.T, norm='ortho', axis=0).T, norm='ortho', axis=0)


def idct2(x): # column wise
    return spfft.idct(spfft.idct(x.T, norm='ortho', axis=0).T, norm='ortho', axis=0)

def reconstruct_image_8():
    """
    Reconstructs image with basis of 8x8 tiles.
    Code partially from https://www.pyrunner.com/weblog/2016/05/26/compressed-sensing-python/
    :return:
    """
    sample_rate = 0.05 #0.25 #1
    tile_size = 8
    # read original image and downsize for speed
    X_orig = Image.open('Male.jpg').convert('L')
    X = spimg.zoom(X_orig, 1) #1/8

    ny, nx = X.shape
    Image.fromarray((X).reshape(ny, nx).T).convert('RGB').save('downsampled.jpeg')

    R = 30
    h = np.random.normal(0, 1, size=(ny,nx))
    X = np.sign(X - np.ones((ny, nx)) * np.mean(X) + R * h)
    #X = np.sign(X - np.ones((ny, nx))*np.mean(X))

    Image.fromarray((X*255).reshape(ny, nx).T).convert('RGB').save('downsampled_quantized.jpeg')
    Image.fromarray((X*255).reshape(ny, nx)).convert('RGB').save('downsampled_quantized2.jpeg')

    # extract small sample of signal
    #k = round(nx * ny * 0.5)  # 50% sample
    #ri = np.random.choice(nx * ny, k, replace=False)  # random sample of indices
    #b = X.T.flat[ri]
    #b = np.expand_dims(b, axis=1)

    # create dct matrix operator using kron (memory errors for large ny*nx)
    A = np.kron(
        spfft.idct(np.identity(tile_size), norm='ortho', axis=0),
        spfft.idct(np.identity(tile_size), norm='ortho', axis=0)
    )

    w = np.ones((tile_size,tile_size))
    for i in range(tile_size):
        for j in range(tile_size):
            w[i,j] = 1+i+j #1 #1+i**2+j**2 #np.log(i+j+np.e) #1+i+j
    w = w.reshape(tile_size*tile_size)
    print("w.reshape(ny,nx): ", w.reshape(tile_size,tile_size).T)

    recovered = np.zeros((ny,nx))
    solution = np.zeros((ny,nx))
    measurement = np.zeros((ny,nx))
    sparse_data = np.zeros((ny,nx))
    for i in range(int(nx/tile_size)):
        for j in range(int(ny / tile_size)):
            left_top = tile_size*i
            left_bottom = tile_size*i+tile_size
            right_top = tile_size * j
            right_bottom = tile_size * j + tile_size

            # extract small sample of signal
            k = round(tile_size * tile_size * sample_rate)  # sample_rate sample
            ri = np.random.choice(tile_size * tile_size, k, replace=False)  # random sample of indices

            b = X[left_top:left_bottom, right_top:right_bottom].T.flat[ri]
            A_temp = A[ri, :]  # same as phi times kron
            #Xat2 = linear_program(A_temp, b, w)
            #Xat2 = hard_thresholding_operator(A_temp, b, w, 8)

            h_temp = h[left_top:left_bottom, right_top:right_bottom].T.flat[ri]
            A_temp = np.c_[A_temp, h_temp * (np.sqrt(np.pi / 2) / len(ri))]
            Xat2 = cone_program(A_temp, b, w, R)

            # reconstruct signal
            Xat = Xat2.reshape(tile_size, tile_size).T  # stack columns
            Xa = idct2(Xat)

            # confirm solution
            if not np.allclose(X.T.flat[ri], Xa.T.flat[ri]):
                print('Warning: values at sample indices don\'t match original.')
            recovered[left_top:left_bottom, right_top:right_bottom] = Xat
            solution[left_top:left_bottom, right_top:right_bottom] = Xa
            measurement[left_top:left_bottom, right_top:right_bottom].T.flat[ri] = 255
            #sparse_data[left_top:left_bottom, right_top:right_bottom] = abs(np.matmul( np.linalg.inv(A), b).reshape(tile_size, tile_size).T)

    w = w.reshape(tile_size, tile_size).T

    #cm = plt.get_cmap('gray')
    #Image.fromarray(cm(solution)).convert('RGB').save('recovered.jpeg')

    # display the result
    #f, ax = plt.subplots(1, 6, figsize=(14, 4))
    #ax[0].imshow(X_orig, cmap='gray', interpolation='none')
    #ax[1].imshow(X, cmap='gray', interpolation='none')
    #ax[2].imshow(measurement, cmap='gray', interpolation='none')
    #ax[3].imshow(recovered, cmap='gray', interpolation='none')
    #ax[4].imshow(solution, cmap='gray', interpolation='none')
    #ax[5].imshow(w, cmap='gray', interpolation='none')

    f, ax = plt.subplots(1, 1, figsize=(4, 4))
    ax.imshow(solution, cmap='gray', interpolation='none')
    plt.axis('off')
    plt.savefig('recovered.png', bbox_inches='tight', dpi=1024)
    plt.show()

    f, ax = plt.subplots(1, 1, figsize=(4, 4))
    ax.imshow(X, cmap='gray', interpolation='none')
    plt.axis('off')
    plt.savefig('X.png', bbox_inches='tight', dpi=1024)
    plt.show()


    #Image.fromarray(X).convert('RGB').save('original.jpeg')
    #Image.fromarray(Xm).convert('RGB').save('sample.jpeg')
    #Image.fromarray(Xa).convert('RGB').save('recovered.jpeg')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print(cvx.installed_solvers())
    #reconstruct_weights_number_measurements("real_trigonometric_polynomials", "CP")
    #for recovery_algorithm in ["HS", "LP", "CP"]:
    #    for measurement_matrix in ["gaussian", "gaussian_circulant", "real_trigonometric_polynomials", "bernoulli"]:
    #        reconstruct_weights_example(measurement_matrix, recovery_algorithm)
    #reconstruct_weights_example("real_trigonometric_polynomials", "CP")
    #reconstruct_function("CP")
    #reconstruct_signal_example()
    #reconstruct_image_example()
    reconstruct_image_8()