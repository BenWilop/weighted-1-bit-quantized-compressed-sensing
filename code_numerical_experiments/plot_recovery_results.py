import numpy as np
import matplotlib.pyplot as plt


def plot_weights_number_measurements(n, number_of_measurements, measurement_matrix, recovery_algorithm,
                                     x_error_vector, x_w_error_vector):
    """
    Comparison of different algorithms / matrices for different weights.
    :param n:
    :param number_of_measurements:
    :param measurement_matrix:
    :param recovery_algorithm:
    :param x_error_vector:
    :param x_w_error_vector:
    :return:
    """
    # plot
    figure, axes = plt.subplots()
    for (m_i, m) in enumerate(number_of_measurements):
        for i in range(len(x_error_vector[0])):
            if np.random.uniform(0,1) > 0.5:
                axes.scatter(m, x_error_vector[m_i][i], color="blue")
                axes.scatter(m, x_w_error_vector[m_i][i], color="orange")
            else:
                axes.scatter(m, x_w_error_vector[m_i][i], color="orange")
                axes.scatter(m, x_error_vector[m_i][i], color="blue")

    mean_x = [np.mean(x_error_vector[m_i]) for m_i in range(len(number_of_measurements))]
    mean_x_w = [np.mean(x_w_error_vector[m_i]) for m_i in range(len(number_of_measurements))]
    axes.plot(number_of_measurements, mean_x, color="blue")
    axes.plot(number_of_measurements, mean_x_w, color="orange")

    plt.title(measurement_matrix + ", " + recovery_algorithm)
    plt.xlabel("number of measurements m")
    plt.ylabel("relative error")
    axes.set(xlim=(0, 1020), ylim=(0, 1.3))
    plt.show()


def plot_recovered_function(f, m, recovery_algorithm, weight_description, x_w_vector, sample_times, h, R):
    """
    plot the recovered function, x_w is recovered vector in real trig basis
    :param f:
    :param m:
    :param recovery_algorithm:
    :param weight_description:
    :param x_w_vector:
    :param sample_times:
    :param h:
    :param R:
    :return:
    """
    # function to evaluate x, x_w
    def g(x, t):
        a = np.zeros(len(x))
        q = int((len(x) - 1) / 2)
        a[0] = 1 #np.sqrt(2)
        for j in range(1,q+1):
            a[2 * j] = np.sqrt(2) * np.cos(2 * np.pi * j * t)
            a[2 * j - 1] = np.sqrt(2) * np.sin(2 * np.pi * j * t)
        return np.dot(a,x)

    # plot
    figure, axes = plt.subplots()
    t = np.linspace(-0.5, 0.5, 1000)
    #for x_w in x_w_vector:
    #    axes.plot(t, [g(x_w, t_i) for t_i in t], color = "blue")
    axes.plot(t, f(t), color="orange")
    for (i,t) in enumerate(sample_times):
        if f(t) + R*h[i] > 0:
            axes.scatter(t, - R*h[i], color="green")
        else:
            axes.scatter(t, - R*h[i], color="red")

    axes.set(xlim=(-0.5, 0.5), ylim=(-1.2, 1.2))
    axes.plot(t, f(t), color="orange")
    plt.title(recovery_algorithm + ", " + weight_description) # + ", " + "\n sin(2*pi*x)+sin(4*pi*x)+sin(6*pi*x)+sin(8*pi*x)"
    plt.show()
